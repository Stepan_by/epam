<%--
  Created by IntelliJ IDEA.
  User: Stepan
  Date: 16.11.2015
  Time: 21:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>
    <title>Option</title>
  </head>
  <body>
    <div class="container">
      <div class="jumbotron">
        <form method="GET" action="XMLParser" class="form-group">
          <label class="label" for="typeSelect">Choose XML Parser</label>
          <select class="form-control" name="type" id="typeSelect">
            <option value="DOM">DOM</option>
            <option value="SAX">SAX</option>
            <option value="STAX">StAX</option>
          </select>
          <label class="label">Choose records per page</label><br/>
          <div class="number-block">
            <input value="1" name="numberPerPage" type="number" min="1">
          </div>
          <input type="submit" value="Run" class="btn btn-primary btn-lg">
        </form>
      </div>
    </div>
  </body>
</html>
