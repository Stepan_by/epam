<%--
  Created by IntelliJ IDEA.
  User: Stepan
  Date: 17.11.2015
  Time: 11:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Result</title>
    <link href="../css/bootstrap.css" rel="stylesheet"/>
    <link href="../css/style.css" rel="stylesheet"/>
</head>
<body>
    <div class="container">
        <div class="back">
            <a href="../index.jsp">
                <button type="button" class="btn btn-lg btn-custom" aria-label="Left Align">
                    <span class="glyphicon glyphicon-triangle-left"></span>
                    Back
                </button>
            </a>
        </div>
        <div class="content">
            <div class="label">
                ${ type }
            </div>
            <table class="table table-bordered table-condensed">
                <thead class="head">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Pharm</th>
                        <th>Group</th>
                        <th>Analogs</th>
                        <th>Version</th>
                        <th>Certificate</th>
                        <th>Package</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="medicine" items="${ medicines }">
                        <tr>
                            <td><c:out value="${ medicine.id }" /></td>
                            <td><c:out value="${ medicine.name }"/></td>
                            <td><c:out value="${ medicine.pharm }"/></td>
                            <td><c:out value="${ medicine.group }"/></td>
                            <td>
                                <c:forEach var="analog" items="${ medicine.analogs }">
                                    <c:out value="${ analog }"/>
                                    <br/>
                                </c:forEach>
                            </td>
                            <td>
                                <c:forEach var="version" items="${ medicine.versions }">
                                    <c:choose>
                                        <c:when test="${ !empty version.pills }">
                                            Pills
                                            <br/><hr/>
                                            <c:forEach var="pills" items="${ version.pills }">
                                                Age : <c:out value="${ pills.age }"/> <br/>
                                                Duration : <c:out value="${ pills.durationInDays }"/> days<br/>
                                                Per day : <c:out value="${ pills.pillsPerDay }"/> pills<br/>
                                                <hr/>
                                            </c:forEach>
                                        </c:when>
                                        <c:when test="${ !empty version.powder }">
                                            Powder
                                            <br/><hr/>
                                            <c:forEach var="powder" items="${ version.powder }">
                                                Age : <c:out value="${ powder.age }"/> <br/>
                                                Duration : <c:out value="${ powder.durationInDays }"/> days<br/>
                                                Per day : <c:out value="${ powder.gramPerDay }"/> gram<br/>
                                                <hr/>
                                            </c:forEach>
                                        </c:when>
                                        <c:when test="${ !empty version.capsules }">
                                            Capsules
                                            <br/><hr/>
                                            <c:forEach var="capsules" items="${ version.capsules }">
                                                Age : <c:out value="${ capsules.age }"/> <br/>
                                                Duration : <c:out value="${ capsules.durationInDays }"/> days<br/>
                                                Per day : <c:out value="${ capsules.capsulesPerDay }"/> capsules<br/>
                                                <hr/>
                                            </c:forEach>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                            </td>
                            <td>
                                Number : <c:out value="${ medicine.certificate.number }"/><br/>
                                From : <c:out value="${ medicine.certificate.from }"/><br/>
                                Until : <c:out value="${ medicine.certificate.until }"/><br/>
                                Organization : <c:out value="${ medicine.certificate.organization }"/>
                            </td>
                            <td>
                                Material : <c:out value="${ medicine.pack.type }"/><br/>
                                Number in package : <c:out value="${ medicine.pack.number }"/><br/>
                                Price : <c:out value="${ medicine.pack.price }"/>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <ul class="pagination pagination-lg">
                <c:choose>
                    <c:when test="${ currentPage > 1 }">
                        <li><a href="<c:url value="/XMLParser?page=${ currentPage - 1 }&numberPerPage=${ number }"/>">&laquo;</a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="disabled"><a href="#">&laquo;</a></li>
                    </c:otherwise>
                </c:choose>
                <c:forEach begin="1" end="${ pages }" var="i">
                    <c:choose>
                        <c:when test="${ currentPage eq i}">
                            <li class="active"><a href="#"> ${ i } </a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="<c:url value="/XMLParser?page=${ i }&numberPerPage=${ number }"/>">${ i }</a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:choose>
                    <c:when test="${ currentPage < pages }">
                        <li><a href="<c:url value="/XMLParser?page=${ currentPage + 1 }&numberPerPage=${ number }"/>">&raquo;</a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="disabled"><a href="#">&raquo;</a></li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </div>
</body>
</html>
