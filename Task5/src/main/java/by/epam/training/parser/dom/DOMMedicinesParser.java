package by.epam.training.parser.dom;


import by.epam.training.model.medicine.*;
import by.epam.training.exception.UnknownVersionType;
import by.epam.training.parser.MedicinesParser;
import by.epam.training.tags.MedicineTags;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class DOMMedicinesParser extends MedicinesParser {

    private static final Logger LOG = Logger.getLogger(DOMMedicinesParser.class);
    private static final String VERSION_PILLS = "pills";
    private static final String VERSION_POWDER = "powder";
    private static final String VERSION_CAPSULES = "capsules";
    private DocumentBuilder builder;

    public DOMMedicinesParser() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error(e + " in creating Document Builder");
        }
    }

    @Override
    public void parseFile(String filePath) {
        try {
            Document document = builder.parse(filePath);
            Element root = document.getDocumentElement();
            NodeList medicines = root.getElementsByTagName(MedicineTags.MEDICINE.getTag());
            for (int i = 0 ; i < medicines.getLength(); i++) {
                Node medicine = medicines.item(i);
                if (medicine.getNodeType() == Node.ELEMENT_NODE) {
                    getMedicines().add(parseMedicine((Element) medicine));
                }
            }
        } catch (SAXException e) {
            LOG.error("Exception in parsing " + e);
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    private Medicine parseMedicine(Element medicineElement) {
        Medicine medicineObject = new Medicine();
        medicineObject.setId(medicineElement.getAttribute(MedicineTags.ID.getTag()));
        medicineObject.setName(firstValueAtTag(medicineElement, MedicineTags.NAME.getTag()));
        medicineObject.setPharm(firstValueAtTag(medicineElement, MedicineTags.PHARM.getTag()));
        medicineObject.setGroup(GroupType.valueOf(firstValueAtTag(medicineElement, MedicineTags.GROUP.getTag()).toUpperCase()));
        NodeList analogs = medicineElement.getElementsByTagName(MedicineTags.ANALOG.getTag());
        for (int i = 0 ; i < analogs.getLength(); i++) {
            medicineObject.getAnalogs().add(analogs.item(i).getTextContent());
        }
        try {
            NodeList versionsTags = medicineElement.getElementsByTagName(MedicineTags.VERSION.getTag());
            for (int i = 0 ; i < versionsTags.getLength(); i++) {
                medicineObject.getVersions().add(parseVersion(versionsTags.item(i)));
            }
        } catch (UnknownVersionType unknownVersionType) {
            LOG.error("Unknown medicine Version " + unknownVersionType);
        }
        medicineObject.setCertificate(parseCertificate((Element)medicineElement.getElementsByTagName(MedicineTags.CERTIFICATE.getTag()).item(0)));
        medicineObject.setPack(parsePackage((Element)medicineElement.getElementsByTagName(MedicineTags.PACKAGE.getTag()).item(0)));
        return medicineObject;
    }

    private CertificateType parseCertificate(Element element) {
        CertificateType certificate = new CertificateType();
        certificate.setNumber(Integer.parseInt(firstValueAtTag(element, MedicineTags.NUMBER.getTag())));
        certificate.setOrganization(firstValueAtTag(element, MedicineTags.ORGANIZATION.getTag()));
        try {
            XMLGregorianCalendar from = DatatypeFactory.newInstance().
                    newXMLGregorianCalendar(firstValueAtTag(element, MedicineTags.FROM.getTag()));
            XMLGregorianCalendar until = DatatypeFactory.newInstance().
                    newXMLGregorianCalendar(firstValueAtTag(element, MedicineTags.UNTIL.getTag()));
            certificate.setFrom(from);
            certificate.setUntil(until);
        } catch (DatatypeConfigurationException e) {
            LOG.error("Error in data type configuration");
        }
        return certificate;
    }

    private PackageType parsePackage(Element element) {
        PackageType medicinePackage = new PackageType();
        medicinePackage.setNumber(Integer.parseInt(firstValueAtTag(element, MedicineTags.NUMBER.getTag())));
        medicinePackage.setPrice(Integer.parseInt(firstValueAtTag(element, MedicineTags.PRICE.getTag())));
        medicinePackage.setType(Material.valueOf(firstValueAtTag(element, MedicineTags.TYPE.getTag()).toUpperCase()));
        return medicinePackage;
    }

    private String firstValueAtTag(Element element, String tag) {
        return element.getElementsByTagName(tag).item(0).getTextContent();
    }

    private VersionType parseVersion(Node version) throws UnknownVersionType {
        VersionType versions = new VersionType();
        String versionAsText = version.getChildNodes().item(1).getNodeName();
        ArrayList<Element> types = takeAllTypesInVersion(version.getChildNodes());
        switch (versionAsText) {
            case VERSION_PILLS:
                versions.getPills().addAll(makeListOfPills(types));
                break;
            case VERSION_POWDER:
                versions.getPowder().addAll(makeListOfPowders(types));
                break;
            case VERSION_CAPSULES:
                versions.getCapsules().addAll(makeListOfCapsules(types));
                break;
            default:
                throw new UnknownVersionType(versionAsText);
        }
        return versions;
    }

    private ArrayList<Element> takeAllTypesInVersion(NodeList types) {
        ArrayList<Element> elements = new ArrayList<>();
        for (int i = 0 ; i < types.getLength(); i++) {
            if (types.item(i).getNodeType() == Node.ELEMENT_NODE) {
                elements.add((Element) types.item(i).getChildNodes());
            }
        }
        return elements;
    }

    private ArrayList<PillsType> makeListOfPills(ArrayList<Element> pills) {
        ArrayList<PillsType> listOfPills = new ArrayList<>();
        for (Element pill : pills) {
            PillsType pillsObject = new PillsType();
            fillGeneralFieldsInVersion(pillsObject, pill);
            pillsObject.setPillsPerDay(Integer.parseInt(firstValueAtTag(pill, MedicineTags.PILLS_PER_DAY.getTag())));
            listOfPills.add(pillsObject);
        }
        return listOfPills;
    }

    private ArrayList<PowderType> makeListOfPowders(ArrayList<Element> powders) {
        ArrayList<PowderType> listOfPowders = new ArrayList<>();
        for (Element powder : powders) {
            PowderType powderObject = new PowderType();
            fillGeneralFieldsInVersion(powderObject, powder);
            powderObject.setGramPerDay(Integer.parseInt(firstValueAtTag(powder, MedicineTags.GRAM_PER_DAY.getTag())));
            listOfPowders.add(powderObject);
        }
        return listOfPowders;
    }

    private ArrayList<CapsulesType> makeListOfCapsules(ArrayList<Element> capsules) {
        ArrayList<CapsulesType> listOfCapsules = new ArrayList<>();
        for (Element capsule : capsules) {
            CapsulesType powderObject = new CapsulesType();
            fillGeneralFieldsInVersion(powderObject, capsule);
            powderObject.setCapsulesPerDay(Integer.parseInt(firstValueAtTag(capsule, MedicineTags.CAPSULES_PER_DAY.getTag())));
            listOfCapsules.add(powderObject);
        }
        return listOfCapsules;
    }

    private void fillGeneralFieldsInVersion(GeneralMedicineType type, Element element) {
        type.setAge(Age.valueOf(firstValueAtTag(element, MedicineTags.AGE.getTag()).toUpperCase()));
        type.setDurationInDays(Integer.parseInt(firstValueAtTag(element, MedicineTags.DURATION.getTag())));
    }
}
