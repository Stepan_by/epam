package by.epam.training.parser.stax;

import by.epam.training.model.medicine.*;
import by.epam.training.parser.MedicinesParser;
import by.epam.training.tags.MedicineTags;
import org.apache.log4j.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class STAXMedicinesParser extends MedicinesParser {

    private MedicineTags currentTag;
    private static final Logger LOG = Logger.getLogger(STAXMedicinesParser.class);

    @Override
    public void parseFile(String filePath) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        try(FileInputStream reader = new FileInputStream(new File(filePath))) {
            XMLStreamReader streamReader = factory.createXMLStreamReader(reader);
            while (streamReader.hasNext()) {
                int event = streamReader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        if (MedicineTags.MEDICINE.getTag().equals(streamReader.getLocalName())) {
                            getMedicines().add(parseMedicine(new Medicine(), streamReader));
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (XMLStreamException e) {
            LOG.error("Stream SAX exception " + e);
        } catch (FileNotFoundException e) {
            LOG.error("File has not been found " + filePath);
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    private Medicine parseMedicine(Medicine medicine, XMLStreamReader reader) throws XMLStreamException {
        medicine.setId(reader.getAttributeValue(0));
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    findAndSetTag(reader.getLocalName());
                    String tagValue = takeTagContent(reader);
                    switch (currentTag) {
                        case NAME:
                            medicine.setName(tagValue);
                            break;
                        case PHARM:
                            medicine.setPharm(tagValue);
                            break;
                        case GROUP:
                            medicine.setGroup(GroupType.valueOf(tagValue.toUpperCase()));
                            break;
                        case ANALOG:
                            medicine.getAnalogs().add(tagValue);
                            break;
                        case CERTIFICATE:
                            medicine.setCertificate(parseCertificate(new CertificateType(), reader));
                            break;
                        case VERSION:
                            medicine.getVersions().add(parseVersion(new VersionType(), reader));
                            break;
                        case PACKAGE:
                            medicine.setPack(parsePackage(new PackageType(), reader));
                            break;
                        default:
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (MedicineTags.MEDICINE.getTag().equals(reader.getLocalName())) {
                        return medicine;
                    }
                    break;
            }
        }
        return null;
    }

    private VersionType parseVersion(VersionType version, XMLStreamReader reader) throws XMLStreamException {
        GeneralMedicineType type = null;
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    findAndSetTag(reader.getLocalName());
                    String tagValue = takeTagContent(reader);
                    switch (currentTag) {
                        case PILLS:
                            type = new PillsType();
                            break;
                        case POWDER:
                            type = new PowderType();
                            break;
                        case CAPSULES:
                            type = new CapsulesType();
                            break;
                        case AGE:
                            type.setAge(Age.valueOf(tagValue.toUpperCase()));
                            break;
                        case DURATION:
                            type.setDurationInDays(Integer.parseInt(tagValue));
                            break;
                        case PILLS_PER_DAY:
                            ((PillsType) type).setPillsPerDay(Integer.parseInt(tagValue));
                            break;
                        case GRAM_PER_DAY:
                            ((PowderType) type).setGramPerDay(Integer.parseInt(tagValue));
                            break;
                        case CAPSULES_PER_DAY:
                            ((CapsulesType) type).setCapsulesPerDay(Integer.parseInt(tagValue));
                            break;
                        default:
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    findAndSetTag(reader.getLocalName());
                    switch (currentTag) {
                        case PILLS:
                            version.getPills().add((PillsType) type);
                            break;
                        case POWDER:
                            version.getPowder().add((PowderType) type);
                            break;
                        case CAPSULES:
                            version.getCapsules().add((CapsulesType) type);
                            break;
                        case VERSION:
                            return version;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        return null;
    }

    private PackageType parsePackage(PackageType pack, XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    findAndSetTag(reader.getLocalName());
                    String tagValue = takeTagContent(reader);
                    switch (currentTag) {
                        case TYPE:
                            pack.setType(Material.valueOf(tagValue.toUpperCase()));
                            break;
                        case NUMBER:
                            pack.setNumber(Integer.parseInt(tagValue));
                            break;
                        case PRICE:
                            pack.setPrice(Integer.parseInt(tagValue));
                            break;
                        default:
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (MedicineTags.PACKAGE.getTag().equals(reader.getLocalName())) {
                        return pack;
                    }
                    break;
                default:
                    break;
            }
        }
        return null;
    }

    private CertificateType parseCertificate(CertificateType certificate, XMLStreamReader reader) throws XMLStreamException{
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    findAndSetTag(reader.getLocalName());
                    switch (currentTag) {
                        case NUMBER:
                            certificate.setNumber(Integer.parseInt(takeTagContent(reader)));
                            break;
                        case ORGANIZATION:
                            certificate.setOrganization(takeTagContent(reader));
                            break;
                        case FROM:case UNTIL:
                            try {
                                XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(takeTagContent(reader));
                                if (currentTag == MedicineTags.FROM) {
                                    certificate.setFrom(date);
                                } else {
                                    certificate.setUntil(date);
                                }
                            } catch (DatatypeConfigurationException e) {
                                LOG.error("Exception int parsing from/until");
                            }
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (MedicineTags.CERTIFICATE.getTag().equals(reader.getLocalName())) {
                        return certificate;
                    }
                default:
                    break;
            }
        }
        return null;
    }

    private void findAndSetTag(String tagText) {
        for (MedicineTags currentTag : MedicineTags.values()) {
            if (currentTag.getTag().equals(tagText)) {
                this.currentTag = currentTag;
                break;
            }
        }
    }

    private String takeTagContent(XMLStreamReader reader) throws XMLStreamException {
        reader.next();
        return reader.getText();
    }
}
