package by.epam.training.parser.sax;

import by.epam.training.model.medicine.*;
import by.epam.training.tags.MedicineTags;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;

public class SAXMedicinesHandler extends DefaultHandler {

    private static final Logger LOG = Logger.getLogger(SAXMedicinesHandler.class);

    private List<Medicine> medicines;
    private Medicine medicine;
    private PackageType medicinePackage;
    private CertificateType certificate;
    private MedicineTags currentTag;
    private VersionType version;
    private MedicineTags[] availableTags;
    private GeneralMedicineType versionType;
    private boolean isPackageActive;

    public List<Medicine> getMedicines() {
        return medicines;
    }

    @Override
    public void startDocument() throws SAXException {
        availableTags = MedicineTags.values();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        findAndSetTag(qName);
        if (currentTag == null) {
            return;
        }
        switch (currentTag) {
            case MEDICINES:
                medicines = new ArrayList<>();
                break;
            case MEDICINE:
                medicine = new Medicine();
                medicine.setId(attributes.getValue("id"));
                break;
            case CERTIFICATE:
                certificate = new CertificateType();
                isPackageActive = false;
                break;
            case PACKAGE:
                medicinePackage = new PackageType();
                isPackageActive = true;
                break;
            case VERSION:
                version = new VersionType();
                break;
            case PILLS:
                versionType = new PillsType();
                break;
            case POWDER:
                versionType = new PowderType();
                break;
            case CAPSULES:
                versionType = new CapsulesType();
                break;
            default:
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        findAndSetTag(qName);
        if (currentTag == null) {
            return;
        }
        switch (currentTag) {
            case MEDICINE:
                medicines.add(medicine);
                break;
            case VERSION:
                medicine.getVersions().add(version);
                break;
            case POWDER:
                version.getPowder().add((PowderType) versionType);
                break;
            case PILLS:
                version.getPills().add((PillsType) versionType);
                break;
            case CAPSULES:
                version.getCapsules().add((CapsulesType) versionType);
                break;
            case CERTIFICATE:
                medicine.setCertificate(certificate);
                break;
            case PACKAGE:
                medicine.setPack(medicinePackage);
                break;
            default:
                break;
        }
        currentTag = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentTag == null) {
            return;
        }
        String tagValue = new String(ch, start, length).trim();
        switch (currentTag) {
            case NAME:
                medicine.setName(tagValue);
                break;
            case PHARM:
                medicine.setPharm(tagValue);
                break;
            case GROUP:
                medicine.setGroup(GroupType.valueOf(tagValue.toUpperCase()));
                break;
            case ANALOG:
                medicine.getAnalogs().add(tagValue);
                break;
            case TYPE:
                medicinePackage.setType(Material.valueOf(tagValue.toUpperCase()));
                break;
            case PRICE:
                medicinePackage.setPrice(Integer.parseInt(tagValue));
                break;
            case NUMBER:
                int number = Integer.parseInt(tagValue);
                if (isPackageActive) {
                    medicinePackage.setNumber(number);
                } else {
                    certificate.setNumber(number);
                }
                break;
            case FROM: case UNTIL:
                try {
                    XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(tagValue);
                    if (currentTag == MedicineTags.FROM) {
                        certificate.setFrom(date);
                    } else {
                        certificate.setUntil(date);
                    }
                } catch (DatatypeConfigurationException e) {
                    LOG.error("Exception int parsing from/until value in ID: " + medicine.getId());
                }
                break;
            case ORGANIZATION:
                certificate.setOrganization(tagValue);
                break;
            case AGE:
                versionType.setAge(Age.valueOf(tagValue.toUpperCase()));
                break;
            case DURATION:
                versionType.setDurationInDays(Integer.parseInt(tagValue.toUpperCase()));
                break;
            case PILLS_PER_DAY:
                ((PillsType) versionType).setPillsPerDay(Integer.parseInt(tagValue));
                break;
            case CAPSULES_PER_DAY:
                ((CapsulesType) versionType).setCapsulesPerDay(Integer.parseInt(tagValue));
                break;
            case GRAM_PER_DAY:
                ((PowderType) versionType).setGramPerDay(Integer.parseInt(tagValue));
                break;
            default:
                break;
        }
    }

    private void findAndSetTag(String tagText) {
        for (MedicineTags currentTag : availableTags) {
            if (currentTag.getTag().equals(tagText)) {
                this.currentTag = currentTag;
                return;
            }
        }
        currentTag = null;
    }
}
