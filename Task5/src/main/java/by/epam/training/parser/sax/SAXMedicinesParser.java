package by.epam.training.parser.sax;

import by.epam.training.parser.MedicinesParser;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class SAXMedicinesParser extends MedicinesParser {

    private static final Logger LOG = Logger.getLogger(SAXMedicinesParser.class);

    @Override
    public void parseFile(String filePath) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXMedicinesHandler handler = new SAXMedicinesHandler();
        LOG.info("SAX parsing started");
        try {
            factory.newSAXParser().parse(filePath, handler);
        } catch (SAXException | ParserConfigurationException | IOException e) {
            LOG.error(e);
        }
        getMedicines().addAll(handler.getMedicines());
        LOG.info("Text parsed by SAX parser successfully and found " +  " elements");
    }
}
