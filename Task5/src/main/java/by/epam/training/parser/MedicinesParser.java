package by.epam.training.parser;

import by.epam.training.model.Medicines;
import by.epam.training.model.medicine.Medicine;

import java.util.List;

public abstract class MedicinesParser {

    private List<Medicine> medicines;

    public MedicinesParser() {
        medicines = Medicines.getInstance().getMedicine();
    }

    public List<Medicine> getMedicines() {
        return medicines;
    }

    abstract public void parseFile(String filePath);
}
