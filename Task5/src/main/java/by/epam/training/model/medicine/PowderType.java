
package by.epam.training.model.medicine;

public class PowderType extends GeneralMedicineType {

    private int gramPerDay;

    public int getGramPerDay() {
        return gramPerDay;
    }

    public void setGramPerDay(int value) {
        this.gramPerDay = value;
    }

}
