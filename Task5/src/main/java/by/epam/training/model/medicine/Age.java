
package by.epam.training.model.medicine;

public enum Age {

    CHILD("Child"),
    ADULT("Adult");

    private final String value;

    Age(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Age fromValue(String v) {
        for (Age c: Age.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
