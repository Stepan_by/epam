
package by.epam.training.model.medicine;

public class CapsulesType extends GeneralMedicineType {

    private Integer capsulesPerDay;

    public int getCapsulesPerDay() {
        return capsulesPerDay;
    }

    public void setCapsulesPerDay(int value) {
        this.capsulesPerDay = value;
    }

}
