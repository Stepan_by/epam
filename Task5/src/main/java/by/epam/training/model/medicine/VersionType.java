
package by.epam.training.model.medicine;

import java.util.ArrayList;
import java.util.List;

public class VersionType {

    private List<PillsType> pills;
    private List<PowderType> powder;
    private List<CapsulesType> capsules;

    public VersionType() {
        pills = new ArrayList<>();
        powder = new ArrayList<>();
        capsules = new ArrayList<>();
    }

    public List<PillsType> getPills() {
        return this.pills;
    }

    public List<PowderType> getPowder() {
        return powder;
    }

    public List<CapsulesType> getCapsules() {
        return capsules;
    }

}
