
package by.epam.training.model.medicine;

public enum Material {

    WOOD("Wood"),
    IRON("Iron"),
    PLASTIC("Plastic");

    private final String value;

    Material(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Material fromValue(String v) {
        for (Material c: Material.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
