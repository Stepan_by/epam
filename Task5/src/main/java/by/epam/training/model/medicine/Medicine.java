
package by.epam.training.model.medicine;

import java.util.ArrayList;
import java.util.List;

public class Medicine {

    private String name;
    private String pharm;
    private GroupType group;
    private List<String> analogs;
    private List<VersionType> versions;
    private CertificateType certificate;
    private PackageType pack;
    private String id;

    public Medicine() {
        analogs = new ArrayList<>();
        versions = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getPharm() {
        return pharm;
    }

    public void setPharm(String value) {
        this.pharm = value;
    }

    public GroupType getGroup() {
        return group;
    }

    public void setGroup(GroupType value) {
        this.group = value;
    }

    public List<String> getAnalogs() {
        return analogs;
    }

    public List<VersionType> getVersions() {
        return versions;
    }

    public CertificateType getCertificate() {
        return certificate;
    }

    public void setCertificate(CertificateType value) {
        this.certificate = value;
    }

    public PackageType getPack() {
        return pack;
    }

    public void setPack(PackageType value) {
        this.pack = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

}
