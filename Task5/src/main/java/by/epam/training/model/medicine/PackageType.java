
package by.epam.training.model.medicine;

public class PackageType {

    private Material type;
    private int number;
    private int price;

    public Material getType() {
        return type;
    }

    public void setType(Material value) {
        this.type = value;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int value) {
        this.number = value;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int value) {
        this.price = value;
    }

}
