
package by.epam.training.model.medicine;

public class GeneralMedicineType {

    protected Age age;
    protected int durationInDays;

    public Age getAge() {
        return age;
    }

    public void setAge(Age value) {
        this.age = value;
    }

    public int getDurationInDays() {
        return durationInDays;
    }

    public void setDurationInDays(int value) {
        this.durationInDays = value;
    }

}
