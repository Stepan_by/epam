
package by.epam.training.model.medicine;

public class PillsType extends GeneralMedicineType {

    private int pillsPerDay;

    public int getPillsPerDay() {
        return pillsPerDay;
    }

    public void setPillsPerDay(int value) {
        this.pillsPerDay = value;
    }

}
