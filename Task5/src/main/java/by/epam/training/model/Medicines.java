
package by.epam.training.model;

import by.epam.training.model.medicine.Medicine;

import java.util.ArrayList;
import java.util.List;

public class Medicines {

    protected List<Medicine> medicines;

    private Medicines(){
        medicines = new ArrayList<>();
    }

    public static class SingletonHolder {
        public static final Medicines INSTANCE = new Medicines();
    }

    public static Medicines getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public List<Medicine> getMedicine() {
        return medicines;
    }

    public void clear() {
        medicines.clear();
    }

}
