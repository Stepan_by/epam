
package by.epam.training.model.medicine;

public enum GroupType {

    ANTIBIOTIC("antibiotic"),
    ANALGESIC("analgesic"),
    VITAMIN("vitamin");
    private final String value;

    GroupType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupType fromValue(String v) {
        for (GroupType c: GroupType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
