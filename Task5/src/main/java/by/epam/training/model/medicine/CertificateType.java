
package by.epam.training.model.medicine;

import javax.xml.datatype.XMLGregorianCalendar;

public class CertificateType {

    private int number;
    private XMLGregorianCalendar from;
    private XMLGregorianCalendar until;
    private String organization;

    public int getNumber() {
        return number;
    }

    public void setNumber(int value) {
        this.number = value;
    }

    public XMLGregorianCalendar getFrom() {
        return from;
    }

    public void setFrom(XMLGregorianCalendar value) {
        this.from = value;
    }

    public XMLGregorianCalendar getUntil() {
        return until;
    }

    public void setUntil(XMLGregorianCalendar value) {
        this.until = value;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String value) {
        this.organization = value;
    }

}
