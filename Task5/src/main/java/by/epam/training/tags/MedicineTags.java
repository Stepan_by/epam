package by.epam.training.tags;

public enum MedicineTags {

    ID("id"),
    AGE("age"),
    ANALOG("analog"),
    CAPSULES("capsules"),
    CAPSULES_PER_DAY("capsulesPerDay"),
    CERTIFICATE("certificate"),
    DURATION("durationInDays"),
    FROM("from"),
    GRAM_PER_DAY("gramPerDay"),
    GROUP("group"),
    MEDICINE("medicine"),
    MEDICINES("medicines"),
    NAME("name"),
    NUMBER("number"),
    ORGANIZATION("organization"),
    PACKAGE("pack"),
    PHARM("pharm"),
    PILLS("pills"),
    PILLS_PER_DAY("pillsPerDay"),
    POWDER("powder"),
    PRICE("price"),
    TYPE("type"),
    UNTIL("until"),
    VERSION("version");

    private String tag;

    MedicineTags(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
