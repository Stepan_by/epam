package by.epam.training.factory;

import by.epam.training.exception.UnknownParserTypeException;
import by.epam.training.parser.dom.DOMMedicinesParser;
import by.epam.training.parser.MedicinesParser;
import by.epam.training.parser.sax.SAXMedicinesParser;
import by.epam.training.parser.stax.STAXMedicinesParser;

public class MedicineBuilderFactory {

    public static class SingletonHolder {
        public static final MedicineBuilderFactory INSTANCE = new MedicineBuilderFactory();
    }

    private MedicineBuilderFactory(){}

    public static MedicineBuilderFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public MedicinesParser createParser(String type) throws UnknownParserTypeException{
        switch (type) {
            case "DOM":
                return new DOMMedicinesParser();
            case "SAX":
                return new SAXMedicinesParser();
            case "STAX":
                return new STAXMedicinesParser();
            default:
                throw new UnknownParserTypeException();
        }
    }
}
