package by.epam.training.exception;

public class UnknownVersionType extends Exception {
    public UnknownVersionType() {
    }

    public UnknownVersionType(String message) {
        super(message);
    }

    public UnknownVersionType(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownVersionType(Throwable cause) {
        super(cause);
    }

    public UnknownVersionType(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
