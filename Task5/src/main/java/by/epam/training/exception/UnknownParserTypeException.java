package by.epam.training.exception;

public class UnknownParserTypeException extends Exception {
    public UnknownParserTypeException() {
    }

    public UnknownParserTypeException(String message) {
        super(message);
    }

    public UnknownParserTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownParserTypeException(Throwable cause) {
        super(cause);
    }

    public UnknownParserTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
