package by.epam.training.servlet;

import by.epam.training.exception.UnknownParserTypeException;
import by.epam.training.factory.MedicineBuilderFactory;
import by.epam.training.model.Medicines;
import by.epam.training.parser.MedicinesParser;
import by.epam.training.viewer.MedicineViewer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ParserServlet", urlPatterns = {"/XMLParser"})
public class ParserServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(ParserServlet.class);
    private static final String FILE_PATH = "data/medicines.xml";
    private static String parserType = "unknown";

    @Override
    public void init() throws ServletException {
        new DOMConfigurator().doConfigure(getServletContext().getRealPath("/")
                + getInitParameter("log4j"), LogManager.getLoggerRepository());
        LOG.info("Parser Servlet initialized successfully");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("GET request");
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("POST request");
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int page = 1;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (request.getParameter("type") != null) {
            if (!parserType.equals(request.getParameter("type"))) {
                parserType = request.getParameter("type");
                LOG.info("User choose " + parserType + " type of parsing");
                parse(parserType);
            }
        }
        int recordsPerPage = Integer.parseInt(request.getParameter("numberPerPage"));
        request.setAttribute("number", recordsPerPage);
        request.setAttribute("medicines", MedicineViewer.viewMedicines((page - 1) * recordsPerPage, recordsPerPage));
        request.setAttribute("pages", countPagesNumber(Medicines.getInstance().getMedicine().size(), recordsPerPage));
        request.setAttribute("currentPage", page);
        request.setAttribute("type", parserType);
        request.getRequestDispatcher("jsp/table.jsp").forward(request, response);
    }

    private void parse(String curParser) {
        MedicineBuilderFactory factory = MedicineBuilderFactory.getInstance();
        try {
            Medicines.getInstance().clear();
            MedicinesParser parser = factory.createParser(curParser);
            parser.parseFile(getServletContext().getRealPath("/" + FILE_PATH));
            LOG.info("File " + FILE_PATH + " parsed with " + curParser + " successfully");
        } catch (UnknownParserTypeException e) {
            LOG.error("Unknown parser type " + e + " " + curParser);
        }
    }

    private int countPagesNumber(double size, int records) {
        return size % records > 0 ? (int) size / records + 1 : (int) size / records;
    }
}
