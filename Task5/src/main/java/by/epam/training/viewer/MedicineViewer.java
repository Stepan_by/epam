package by.epam.training.viewer;

import by.epam.training.model.Medicines;
import by.epam.training.model.medicine.Medicine;
import org.apache.log4j.Logger;

import java.util.List;


public class MedicineViewer {

    private static final Logger LOG = Logger.getLogger(MedicineViewer.class);

    public static List<Medicine> viewMedicines(int offset, int noOfRecords) {
        List<Medicine> medicines = Medicines.getInstance().getMedicine();
        int lastElement = offset + noOfRecords > medicines.size() ? medicines.size() : offset + noOfRecords;
        LOG.info("Records from " + offset + " to " + lastElement + " have been given");
        return medicines.subList(offset, lastElement);
    }
}
