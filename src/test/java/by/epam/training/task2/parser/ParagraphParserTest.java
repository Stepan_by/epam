package by.epam.training.task2.parser;

import by.epam.training.task2.composite.ComponentType;
import by.epam.training.task2.composite.Composite;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ParagraphParserTest {

    private static final String TEXT = "The are a entity of sentences here. The first: first; The second: second?";

    @Test
    public void testParseParagraph() throws Exception {
        Composite sentence = new Composite(ComponentType.SENTENCE);
        ParagraphParser.parseParagraph(sentence, TEXT);
        int actualChildrenNumber = sentence.countChild();
        int expectedChildrenNumber = 2;
        Assert.assertEquals(expectedChildrenNumber, actualChildrenNumber);
    }
}