package by.epam.training.task2.parser;

import by.epam.training.task2.composite.Composite;
import org.junit.Assert;
import org.junit.Test;

public class TextParserTest {

    private static final String TEXT = "\t1.First paragraph.\n\t2.Second paragraph\n\t";

    @Test
    public void testParseText() throws Exception {
        Composite ct = TextParser.parseText(TEXT);
        int actualChildrenNumber = ct.countChild();
        int expectedChildrenNumber = 2;
        Assert.assertEquals(expectedChildrenNumber, actualChildrenNumber);
    }
}