package by.epam.training.task2.parser;

import by.epam.training.task2.composite.ComponentType;
import by.epam.training.task2.composite.Composite;
import org.junit.Assert;
import org.junit.Test;

public class SentenceParserTest {

    private static final String TEXT = "The are a entity of sentences here. The first: first; The second: second? ";

    @Test
    public void testParseSentence() throws Exception {
        Composite sentence = new Composite(ComponentType.SENTENCE);
        SentenceParser.parseSentence(sentence, TEXT);
        int actualChildrenNumber = sentence.countChild();
        int expectedChildrenNumber = 18;
        Assert.assertEquals(expectedChildrenNumber, actualChildrenNumber);
    }
}