package by.epam.training.task3.auction.entity;

import org.apache.log4j.Logger;

import java.util.Random;

public class Lot {

    private int ID;
    private int cost;
    private int step;
    private String leaderName;
    private LotLifeThread time;
    private static final Logger LOG = Logger.getLogger(Lot.class);

    public Lot(int ID, int cost, int time) {
        this.ID = ID;
        this.cost = cost;
        this.step = (int)(1 + new Random().nextDouble() % 10) * cost;
        this.time = new LotLifeThread(time);
    }

    public void incCost(int increment) {
        cost += increment;
        leaderName = Thread.currentThread().getName();
    }

    public int getStep() {
        return step;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void stopLotLife() {
        time.interrupt();
    }

    public boolean isLeader(String name) {
        return name.equals(leaderName);
    }

    public boolean isAvailable() {
        if (time.getState().equals(Thread.State.NEW)) {
            time.setDaemon(true);
            time.setPriority(Thread.MAX_PRIORITY);
            time.start();
            LOG.info("Lot " + ID + " starting to live");
        }
        return time.isAlive();
    }

    @Override
    public String toString() {
        return String.format("Lot ID: %d, cost : %d, auction step : %d", ID, cost, step);
    }
}
