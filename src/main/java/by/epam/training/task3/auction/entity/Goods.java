package by.epam.training.task3.auction.entity;

import by.epam.training.task3.main.Main;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Goods {

    private ArrayList<Lot> lots;
    private Lock lock;
    private static Logger LOG = Logger.getLogger(Goods.class);

    public Goods(int number) {
        lock = new ReentrantLock(true);
        lots = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            Random random = new Random();
            lots.add(new Lot(random.nextInt(100), random.nextInt(100) + 1, random.nextInt(3000)));
        }
        LOG.info("Lots in " + Main.NUMBER_OF_LOTS + " initialized");
    }

    public boolean isLotAvailable(int index) {
        lock.lock();
        try {
            return lots.get(index).isAvailable();
        } finally {
            lock.unlock();
        }
    }

    public void stopLot(int index) {
        lots.get(index).stopLotLife();
    }

    public int getSize() {
        return lots.size();
    }

    public int getStep(int index) {
        return lots.get(index).getStep();
    }

    public int incLotCost(int index) {
        try {
            lock.lock();
            Lot lot = lots.get(index);
            if (!lot.isLeader(Thread.currentThread().getName()) && lot.isAvailable()) {
                lot.incCost(getStep(index));
                LOG.info(Thread.currentThread().getName() + " say " + lot.getCost());
            }
            return lot.getCost();
        } finally {
            lock.unlock();
        }
    }
}
