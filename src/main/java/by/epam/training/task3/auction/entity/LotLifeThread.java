package by.epam.training.task3.auction.entity;

import by.epam.training.task3.auction.Auction;
import by.epam.training.task3.enums.LotState;
import org.apache.log4j.Logger;

public class LotLifeThread extends Thread {

    private int livingTime;
    private static final Logger LOG = Logger.getLogger(LotLifeThread.class);

    public LotLifeThread(int livingTime) {
        this.livingTime = livingTime;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(livingTime);
            Auction.currentLotState = LotState.TIME_FINISHED;
        } catch (InterruptedException e) {
            LOG.info("Someone won this lot!");
        }
    }
}