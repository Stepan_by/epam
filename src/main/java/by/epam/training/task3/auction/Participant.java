package by.epam.training.task3.auction;

import by.epam.training.task3.enums.LotState;
import by.epam.training.task3.auction.entity.Goods;
import by.epam.training.task3.main.Main;
import org.apache.log4j.Logger;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Participant extends Thread {

    public static int deniedParticipantCounter;
    private static final int TIME_FOR_THINKING = 100;
    private CyclicBarrier barrier;
    private Goods lots;
    private int price;
    private static Logger LOG = Logger.getLogger(Participant.class);

    public Participant(String name, CyclicBarrier barrier, Goods lots) {
        super(name);
        this.barrier = barrier;
        this.lots = lots;
    }

    @Override
    public void run() {
        for (int i = 0; i < lots.getSize(); i++) {
            price = 0;
            while (isContinue() && lots.isLotAvailable(i)) {
                try {
                    Thread.sleep(TIME_FOR_THINKING);
                } catch (InterruptedException e) {
                    LOG.error(Thread.currentThread().getName() + " interrupted");
                }
                price = lots.incLotCost(i);
                yield();
            }
            try{
                if (lots.isLotAvailable(i)) {
                    if (price != 0) {
                        LOG.info(Thread.currentThread().getName() + " is waiting..");
                    } else {
                        LOG.info(Thread.currentThread().getName() + " denied to buy this lot");
                        deniedParticipantCounter ++;
                        if (deniedParticipantCounter == Main.NUMBER_OF_PARTICIPANTS) {
                            Auction.currentLotState = LotState.NOT_INTERESTING;
                        }
                    }
                }
                barrier.await();
            } catch (BrokenBarrierException e) {
                LOG.error("BarrierAction was broken");
            } catch (InterruptedException e) {
                LOG.error(Thread.currentThread().getName() + " was interrupted");
            }
        }
    }

    private boolean isContinue() {
        return new Random().nextBoolean();
    }

    public int getPrice() {
        return price;
    }
}
