package by.epam.training.task3.auction;

import by.epam.training.task3.auction.entity.Goods;
import by.epam.training.task3.enums.LotState;
import by.epam.training.task3.main.Main;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;

public class Auction {
    public static LotState currentLotState;
    private static final Logger LOG = Logger.getLogger(Auction.class);
    private CyclicBarrier barrier;
    private Goods lots;
    private ArrayList<Participant> participants;

    public Auction() {
        currentLotState = LotState.ACTIVE;
        lots = new Goods(Main.NUMBER_OF_LOTS);
        BarrierAction barrierAction = new BarrierAction(lots);
        barrier = new CyclicBarrier(Main.NUMBER_OF_PARTICIPANTS, barrierAction);
        createParticipants(Main.NUMBER_OF_PARTICIPANTS);
        barrierAction.setParticipants(participants);
    }

    public void startAuction() {
        participants.forEach(Participant::start);
        LOG.info("Auction started\n");
        for (Participant participant : participants) {
            try {
                participant.join();
            } catch (InterruptedException e) {
                LOG.error("Error in joining participants " + e.toString());
            }
        }
        LOG.info("Auction ended\n");
    }

    private void createParticipants(int number) {
        participants = new ArrayList<>();
        for (int i = 0 ; i < number; i++) {
            participants.add(new Participant("Participant №" + (i + 1), barrier, lots));
        }
        LOG.info("All participants registered\n");
    }
}
