package by.epam.training.task3.auction;

import by.epam.training.task3.auction.entity.Goods;
import by.epam.training.task3.enums.LotState;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class BarrierAction implements Runnable {

    private ArrayList<Participant> participants;
    private Goods lots;
    private static int numberOfCurrentLot;
    private static final Logger LOG = Logger.getLogger(BarrierAction.class);

    public BarrierAction(Goods lots) {
        this.lots = lots;
    }

    public void setParticipants(ArrayList<Participant> participants) {
        this.participants = participants;
    }

    @Override
    public void run() {
        lots.stopLot(numberOfCurrentLot++);
        switch (Auction.currentLotState) {
            case ACTIVE:
                LOG.info(participants.parallelStream()
                        .max((p1, p2) -> p1.getPrice() - p2.getPrice())
                        .get().getName() + " won!\n");
                break;
            case NOT_INTERESTING:
                LOG.info("Current entity is not interesting\n");
                break;
            case TIME_FINISHED:
                LOG.info("Time finished\n");
                break;
            default:
                LOG.error("Unknown option");
        }
        Auction.currentLotState = LotState.ACTIVE;
        Participant.deniedParticipantCounter = 0;
    }
}
