package by.epam.training.task3.main;

import by.epam.training.task3.auction.Auction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class Main {

    static {
        new DOMConfigurator().doConfigure("config\\log4j.xml", LogManager.getLoggerRepository());
    }

    private static Logger LOG = Logger.getLogger(Main.class);
    public static final int NUMBER_OF_LOTS = 10;
    public static final int NUMBER_OF_PARTICIPANTS = 10;

    public static void main(String[] args) {
        LOG.info("Application started");
        new Auction().startAuction();
    }
}
