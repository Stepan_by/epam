package by.epam.training.task3.enums;

public enum LotState {
    ACTIVE, TIME_FINISHED, NOT_INTERESTING;
}
