package by.epam.training.task2.action;

import by.epam.training.task2.composite.ComponentType;
import by.epam.training.task2.composite.Composite;
import by.epam.training.task2.composite.Word;

import java.util.*;

public class TextAction {

    public static Set<String> findWords(int number, Composite composite) {
        Set<String> words = new HashSet<>();
        for (Composite sentence : takeAllSentences(composite)) {
            if (sentence.getChild(sentence.countChild()-1).toString().contains("?")) {
                for (int k = 0; k < sentence.countChild(); k++) {
                    if (sentence.getChild(k).toString().length() - 1 == number) {
                        words.add(sentence.getChild(k).toString());
                    }
                }
            }
        }
        return words;
    }

    public static Composite removeLines(String startSymbol, String lastSymbol, Composite composite) {
        Set<String> words = new HashSet<>();
        Composite text = (Composite) composite.clone();
        for (Composite sentence : takeAllSentences(text)) {
            for (int i = 0 ; i < sentence.countChild(); i++) {
                String child = sentence.getChild(i).toString();
                child = child.substring(1, child.length());
                if (child.startsWith(startSymbol) && child.endsWith(lastSymbol)) {
                    words.add(child);
                }
            }
            if (words.size() == 0) {
                continue;
            }
            Optional<String> optional = words.stream().sorted((a, b) -> b.length() - a.length()).findFirst();
            for (int i = 0 ; i < sentence.countChild(); i++) {
                String child = sentence.getChild(i).toString();
                child = child.substring(1, child.length());
                if (child.equals(optional.get())) {
                    sentence.remove(i);
                }
            }
            words.clear();
        }
        return text;
    }

    public static Composite changeSubSequence(int numberOfSentence, int wordLength, String subSequence, Composite composite) {
        Composite text = (Composite) composite.clone();
        Composite line = takeAllSentences(composite).get(numberOfSentence);
        for (int i = 0 ; i < line.countChild(); i++) {
            String child = line.getChild(i).toString();
            if (child.length()-1 == wordLength) {
                line.setChild(i, new Word(subSequence));
            }
        }
        return text;
    }

    private static ArrayList<Composite> takeAllSentences(Composite text) {
        ArrayList<Composite> sentences = new ArrayList<>();
        for (int i = 0; i < text.countChild(); i++) {
            Composite paragraph = (Composite) text.getChild(i);
            for (int j = 0; j < paragraph.countChild(); j++) {
                if (paragraph.getChild(j).getComponentType().equals(ComponentType.SENTENCE)) {
                    sentences.add((Composite) paragraph.getChild(j));
                }
            }
        }
        return sentences;
    }
}
