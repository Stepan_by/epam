package by.epam.training.task2.main;

import by.epam.training.task1.exception.FileException;
import by.epam.training.task2.action.TextAction;
import by.epam.training.task2.composite.Composite;
import by.epam.training.task2.parser.TextParser;
import by.epam.training.task2.reader.TextReader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.sound.midi.Soundbank;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public class Main {

    static {
        new DOMConfigurator().doConfigure("config\\log4j.xml", LogManager.getLoggerRepository());
    }

    private static final String TEXT_PATH = "src\\by\\epam\\training\\task2\\text\\text.txt";
    private static final Logger LOG = Logger.getLogger(Main.class);
    private static final String OUTPUT_FILE = "output.txt";
    private static final String START_SYMBOL = "s";
    private static final String END_SYMBOL = "s";
    private static final String SUB_SEQUENCE = "TEST";
    private static final int sentenceNumber = 1;
    private static final int wordLength = 9;

    public static void main(String[] args) {
        try {
            Composite text = TextParser.parseText(TextReader.readTextFromTxt(TEXT_PATH));
            Set words = TextAction.findWords(4, text);
            Composite textAfterTask11 = TextAction.removeLines(START_SYMBOL, END_SYMBOL, text);
            Composite textAfterTask16 = TextAction.changeSubSequence(sentenceNumber, wordLength, SUB_SEQUENCE, text);
            try(FileWriter writer = new FileWriter(OUTPUT_FILE)) {
                writer.write("Text after rebuilding : \n" + text.toString());
                writer.write("Task 4 : \n" + words + "\n");
                writer.write("Task 11 : \n" + textAfterTask11 + "\n");
                writer.write("Task 16 : \n" + textAfterTask16 + "\n");
            } catch (IOException e) {
                LOG.error("Error in writing to file");
            }
        } catch (FileException e) {
            LOG.error(e.getMessage());
        }
    }
}
