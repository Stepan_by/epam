package by.epam.training.task2.reader;

import by.epam.training.task1.exception.FileException;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TextReader {

    private static Logger LOG = Logger.getLogger(TextReader.class);

    public static String readTextFromTxt(String filePath) throws FileException {
        StringBuilder builder = new StringBuilder();
        try {
            Files.lines(Paths.get(filePath), StandardCharsets.UTF_8).forEach(line -> builder.append(line).append("\n"));
            LOG.info("Text has been successfully read");
        } catch (FileNotFoundException e) {
            throw new FileException("File has not been found");
        } catch (IOException e) {
            throw new FileException("File reading error");
        } catch (Exception e) {
            throw new FileException("Unknown exception");
        }
        return builder.toString();
    }
}
