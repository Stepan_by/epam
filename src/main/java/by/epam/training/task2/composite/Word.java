package by.epam.training.task2.composite;

public class Word implements Component {

    private ComponentType type = ComponentType.WORD;
    private String word;

    public Word() {
    }

    public Word(String word) {
        this.word = word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public ComponentType getComponentType() {
        return type;
    }

    @Override
    public String toString() {
        return " " + word;
    }
}
