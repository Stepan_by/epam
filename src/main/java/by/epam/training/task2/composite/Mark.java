package by.epam.training.task2.composite;

public class Mark implements Component {

    private ComponentType type = ComponentType.MARK;
    private String mark;

    public Mark() {
    }

    public Mark(String mark) {
        this.mark = mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public ComponentType getComponentType() {
        return type;
    }

    @Override
    public String toString() {
        return mark;
    }
}
