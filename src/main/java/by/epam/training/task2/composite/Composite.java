package by.epam.training.task2.composite;

import by.epam.training.task2.exception.WrongIndexException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Composite implements Component, Cloneable {

    private ArrayList<Component> components;
    private ComponentType type;
    private static final Logger LOG = Logger.getLogger(Composite.class);

    public Composite() {
        this.components = new ArrayList<>();
    }

    public Composite(ComponentType type) {
        components = new ArrayList<>();
        this.type = type;
    }

    public Composite(ArrayList<Component> components, ComponentType type) {
        this.components = components;
        this.type = type;
    }

    public void add(Component component) {
        components.add(component);
    }

    public void remove(int index) throws WrongIndexException {
        if (index < components.size()) {
            components.remove(index);
        } else {
            LOG.error("Wrong index in getting child");
            throw new WrongIndexException("Wrong index");
        }
    }

    public int countChild() {
        return components.size();
    }


    public void setComponents(ArrayList<Component> components) {
        this.components = components;
    }

    public void addComponents(List<Component> components) {
        this.components.addAll(components);
    }

    public Component getChild(int index) throws WrongIndexException {
        if (index < components.size()) {
            return components.get(index);
        } else {
            LOG.error("Wrong index in getting child");
            throw new WrongIndexException("Wrong index");
        }
    }

    public void setChild(int index, Component component) throws WrongIndexException {
        if (index < components.size()) {
            components.set(index, component);
        } else {
            LOG.error("Wrong index in setting child");
            throw new WrongIndexException("Wrong index");
        }
    }

    public void setType(ComponentType type) {
        this.type = type;
    }

    @Override
    public ComponentType getComponentType() {
        return type;
    }

    @Override
    public Object clone() {
        Composite copy = null;
        try {
            copy = (Composite) super.clone();
            copy.components = (ArrayList<Component>)this.components.clone();
        } catch (CloneNotSupportedException e) {
            LOG.error("Composite can't be clone");
        }
        return copy;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        components.forEach(builder::append);
        return builder.toString();
    }
}
