package by.epam.training.task2.composite;

public interface Component {
    ComponentType getComponentType();
}
