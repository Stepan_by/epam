package by.epam.training.task2.composite;

public class Listing implements Component{

    private ComponentType type = ComponentType.LISTING;
    private String listing;

    public Listing(String listing) {
        this.listing = listing;
    }

    @Override
    public ComponentType getComponentType() {
        return type;
    }

    @Override
    public String toString() {
        return listing;
    }
}
