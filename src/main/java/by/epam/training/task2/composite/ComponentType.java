package by.epam.training.task2.composite;

public enum ComponentType {
    TEXT, PARAGRAPH, LISTING, SENTENCE, WORD, MARK
}
