package by.epam.training.task2.parser;

import by.epam.training.task2.composite.Component;
import by.epam.training.task2.composite.ComponentType;
import by.epam.training.task2.composite.Composite;
import by.epam.training.task2.composite.Listing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser {

    private static final String TITLE = "(\\d\\.)+?\\s.*\\n";
    private static final String SENTENCE_EXP = "(.|\\s)*?(\\.|\\?)\\s";
    private static final String LISTING_EXP = "//Begin(.|\\n)*?//End";
    private static int positionInParsing;

    public static void parseParagraph(Composite paragraph, String textParagraph) {
        paragraph.add(parseTitle(textParagraph));
        Pattern listingPattern = Pattern.compile(LISTING_EXP);
        Pattern sentencePattern = Pattern.compile(SENTENCE_EXP);
        Matcher matcher = sentencePattern.matcher(textParagraph);
        while (matcher.find(positionInParsing)) {
            String parsedText = matcher.group();
            Matcher listingMatcher = listingPattern.matcher(parsedText);
            Composite sentence = new Composite(ComponentType.SENTENCE);
            if (listingMatcher.find()) {
                SentenceParser.parseSentence(sentence, parsedText.substring(0, listingMatcher.start()));
                sentence.add(new Listing(listingMatcher.group()));
                SentenceParser.parseSentence(sentence, parsedText.substring(listingMatcher.end(), parsedText.length()));
            } else {
                SentenceParser.parseSentence(sentence, parsedText);
            }
            paragraph.add(sentence);
            positionInParsing = matcher.end();
        }
    }

    private static Component parseTitle(String text) {
        Composite title = new Composite(ComponentType.SENTENCE);
        Pattern titlePattern = Pattern.compile(TITLE);
        Matcher matcher = titlePattern.matcher(text);
        if (matcher.find()) {
            SentenceParser.parseSentence(title, matcher.group());
            positionInParsing = matcher.end();
        }
        return title;
    }
}
