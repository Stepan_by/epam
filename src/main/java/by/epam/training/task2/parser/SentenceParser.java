package by.epam.training.task2.parser;

import by.epam.training.task2.composite.Composite;
import by.epam.training.task2.composite.Mark;
import by.epam.training.task2.composite.Word;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser {

    private static final String WORD_EXP = "(\\w|\\S)*\\s";
    private static final String PUNCTUATION_EXP = "[:;,.?\n]\\n?";
    private static final String PARAGRAPH_NUMBER = "(\\d\\.)+?\\s";

    public static void parseSentence(Composite sentence, String textSentence) {
        Pattern sentencePattern = Pattern.compile(WORD_EXP);
        Matcher matcher = sentencePattern.matcher(textSentence);
        int position = 0;
        while (matcher.find(position)) {
            String parsedWord = matcher.group();
            Matcher punctuationMatcher = Pattern.compile(PUNCTUATION_EXP).matcher(parsedWord);
            if (punctuationMatcher.find()) {
                if (Pattern.matches(PARAGRAPH_NUMBER, parsedWord)) {
                    sentence.add(new Word(parsedWord));
                    position += parsedWord.length();
                    continue;
                } else {
                    sentence.add(new Word(parsedWord.substring(0, punctuationMatcher.start())));
                    sentence.add(new Mark(punctuationMatcher.group()));
                }
            } else {
                sentence.add(new Word(parsedWord.trim()));
            }
            position = matcher.end();
        }
    }
}
