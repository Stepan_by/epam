package by.epam.training.task2.parser;

import by.epam.training.task2.composite.ComponentType;
import by.epam.training.task2.composite.Composite;

import javax.jws.soap.SOAPBinding;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {

    private static final String PARAGRAPH_EXP = "(\\d.)+?(.|\\n)+?\\n\\t(\\d.)*?";

    public static Composite parseText(String text) {
        Composite composite = new Composite(ComponentType.TEXT);
        Pattern paragraphPattern = Pattern.compile(PARAGRAPH_EXP);
        Matcher matcher = paragraphPattern.matcher(text);
        int position = 0;
        while (matcher.find(position)) {
            String parsedText = matcher.group();
            Composite componentParagraph = new Composite(ComponentType.PARAGRAPH);
            ParagraphParser.parseParagraph(componentParagraph, parsedText);
            composite.add(componentParagraph);
            position = matcher.end();
        }
        return composite;
    }
}
