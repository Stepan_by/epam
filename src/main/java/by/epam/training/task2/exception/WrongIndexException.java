package by.epam.training.task2.exception;

public class WrongIndexException extends IndexOutOfBoundsException {
    public WrongIndexException() {
    }

    public WrongIndexException(String s) {
        super(s);
    }
}
