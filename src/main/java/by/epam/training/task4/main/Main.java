package by.epam.training.task4.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Main {

    static {
        new DOMConfigurator().doConfigure("config\\log4j.xml", LogManager.getLoggerRepository());
    }

    private static final Logger LOG = Logger.getLogger(Main.class);
    private static final String TRANSFORMER_XSL = "src\\by\\epam\\training\\task4\\transformator\\medicins.xsl";
    private static final String INFORMATION_XML = "src\\by\\epam\\training\\task4\\data\\medicins.xml";
    private static final String RESULT_XML = "src\\by\\epam\\training\\task4\\result\\outmedicins.xml";

    public static void main(String[] args) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(TRANSFORMER_XSL));
            transformer.transform(new StreamSource(INFORMATION_XML), new StreamResult(RESULT_XML));
            LOG.info("Transformation success from " + INFORMATION_XML + " to " + RESULT_XML);
        } catch (TransformerException exception) {
            LOG.error("Transformer exception " + exception);
        }
    }
}
