<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" />
	
	<xsl:template match="/">
        <medicins>
            <xsl:apply-templates />
        </medicins>
	</xsl:template>

    <xsl:template match="medicine">
        <medicine id="{@id}">
            <name><xsl:value-of select="name"/></name>
            <pharm><xsl:value-of select="pharm"/></pharm>
            <group><xsl:value-of select="group"/></group>
            <analog><xsl:value-of select="analog"/></analog>
            <version><xsl:value-of select="version"/></version>
            <certificate><xsl:value-of select="certificateion"/></certificate>
            <package><xsl:value-of select="package"/></package>
        </medicine>
    </xsl:template>
</xsl:stylesheet>