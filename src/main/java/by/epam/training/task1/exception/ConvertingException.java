package by.epam.training.task1.exception;

public class ConvertingException extends Exception {
    public ConvertingException() {
    }

    public ConvertingException(String message) {
        super(message);
    }

    public ConvertingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConvertingException(Throwable cause) {
        super(cause);
    }

    public ConvertingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
