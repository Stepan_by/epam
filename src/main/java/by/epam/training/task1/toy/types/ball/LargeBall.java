package by.epam.training.task1.toy.types.ball;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Ball;

public final class LargeBall extends Ball {

    public LargeBall() throws DataException {
        setMaterial("Rubber");
        setCost(100);
        setSize(Size.LARGE);
        setRadius(30);
    }
}
