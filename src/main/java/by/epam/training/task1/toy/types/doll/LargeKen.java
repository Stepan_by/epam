package by.epam.training.task1.toy.types.doll;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Doll;

public final class LargeKen extends Doll {

    public LargeKen() throws DataException {
        setName("Ken Carson");
        setHearColor("BLACK");
        setSize(Size.LARGE);
        setCost(150);
    }
}
