package by.epam.training.task1.toy.types.ball;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Ball;

public final class SmallBall extends Ball {

    public SmallBall() throws DataException {
        setMaterial("Wood");
        setCost(10);
        setSize(Size.SMALL);
        setRadius(2);
    }
}
