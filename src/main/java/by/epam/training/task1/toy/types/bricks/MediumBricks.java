package by.epam.training.task1.toy.types.bricks;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Bricks;

public final class MediumBricks extends Bricks {

    public MediumBricks() throws DataException {
        setSize(Size.MEDIUM);
        setCost(50);
        setMaterial("Wood");
        setNumber(15);
    }
}
