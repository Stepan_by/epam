package by.epam.training.task1.toy.types;

import by.epam.training.task1.toy.Toy;

public abstract class Car extends Toy {

    private String model;
    private int numberOfWheels;
    private String color;

    public Car() {
        setType(Type.CAR);
    }

    public Car(int cost, Size size, Type type, String model, int numberOfWheels, String color) {
        super(cost, size, type);
        this.model = model;
        this.numberOfWheels = numberOfWheels;
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(", Color - %s, Model - %s, Number of wheels - %d;\n",
                getColor(), getModel(), getNumberOfWheels());
    }
}
