package by.epam.training.task1.toy.types.car;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Car;

public final class SimpleMediumCar extends Car {

    public SimpleMediumCar() throws DataException {
        setNumberOfWheels(4);
        setSize(Size.MEDIUM);
        setColor("WHITE");
        setCost(200);
        setModel("FERRARI");
    }
}
