package by.epam.training.task1.toy.types;

import by.epam.training.task1.toy.Toy;

public abstract class Bricks extends Toy {

    private String material;
    private int number;

    public Bricks() {
        setType(Type.BRICKS);
    }

    public Bricks(int cost, Size size, Type type, String material, int number) {
        super(cost, size, type);
        this.material = material;
        this.number = number;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(", Material - %s, Number - %d;\n", getMaterial(), getNumber());
    }
}
