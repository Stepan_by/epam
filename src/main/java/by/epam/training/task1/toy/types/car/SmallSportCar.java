package by.epam.training.task1.toy.types.car;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Car;

public final class SmallSportCar extends Car {

    public SmallSportCar() throws DataException {
        setNumberOfWheels(4);
        setSize(Size.SMALL);
        setColor("BLACK");
        setCost(80);
        setModel("Lamborghini");
    }
}
