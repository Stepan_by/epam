package by.epam.training.task1.toy.types.doll;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Doll;

public final class SmallDoll extends Doll {

    public SmallDoll() throws DataException {
        setName("Small old doll");
        setHearColor("RED");
        setSize(Size.SMALL);
        setCost(35);
    }
}
