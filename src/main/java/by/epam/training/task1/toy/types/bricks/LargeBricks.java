package by.epam.training.task1.toy.types.bricks;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Bricks;

public final class LargeBricks extends Bricks {

    public LargeBricks() throws DataException {
        setSize(Size.LARGE);
        setCost(150);
        setMaterial("Wood");
        setNumber(30);
    }
}
