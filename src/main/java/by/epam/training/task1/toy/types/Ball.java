package by.epam.training.task1.toy.types;

import by.epam.training.task1.toy.Toy;

public abstract class Ball extends Toy {

    private String material;
    private double radius;

    public Ball() {
        setType(Type.BALL);
    }

    public Ball(int cost, Size size, Type type, String material, double radius) {
        super(cost, size, type);
        this.material = material;
        this.radius = radius;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(", Material - %s, Radius - %.2f;\n", getMaterial(), getRadius());
    }
}
