package by.epam.training.task1.toy.types.ball;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Ball;

public final class MediumBall extends Ball {

    public MediumBall() throws DataException {
        setMaterial("Rubber");
        setCost(40);
        setSize(Size.MEDIUM);
        setRadius(10);
    }
}
