package by.epam.training.task1.toy.types.car;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Car;

public final class LargeBus extends Car {

    public LargeBus() throws DataException {
        setCost(100);
        setColor("WHITE");
        setModel("School bus");
        setNumberOfWheels(4);
        setSize(Size.LARGE);
    }

}
