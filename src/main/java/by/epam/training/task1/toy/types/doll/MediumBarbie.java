package by.epam.training.task1.toy.types.doll;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Doll;

public final class MediumBarbie extends Doll {

    public MediumBarbie() throws DataException {
        setCost(100);
        setSize(Size.MEDIUM);
        setHearColor("YELLOW");
        setName("Barbara Millicent Roberts");
    }
}
