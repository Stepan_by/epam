package by.epam.training.task1.toy;

import by.epam.training.task1.exception.DataException;

public abstract class Toy {

    public enum Size {
        ANY,
        SMALL,
        MEDIUM,
        LARGE
    }
    public enum Type {
        ANY,
        BALL,
        BRICKS,
        CAR,
        DOLL
    }
    private int cost;
    private Size size;
    private Type type;

    public Toy() {}

    public Toy(int cost, Size size, Type type) {
        this.cost = cost;
        this.size = size;
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) throws DataException {
        if (cost < 0) {
            throw new DataException("Toy " + type + " cost below zero");
        }
        this.cost = cost;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("%s : Cost - %s, Size - %s", getType(), getCost(), getSize());
    }
}
