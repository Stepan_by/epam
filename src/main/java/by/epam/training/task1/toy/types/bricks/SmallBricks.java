package by.epam.training.task1.toy.types.bricks;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.toy.types.Bricks;

public final class SmallBricks extends Bricks {

    public SmallBricks() throws DataException {
        setSize(Size.SMALL);
        setCost(20);
        setMaterial("Wood");
        setNumber(10);
    }
}
