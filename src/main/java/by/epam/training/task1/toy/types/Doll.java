package by.epam.training.task1.toy.types;

import by.epam.training.task1.toy.Toy;

public abstract class Doll extends Toy {

    private String name;
    private String hearColor;

    public Doll() {
        setType(Type.DOLL);
    }

    public Doll(int cost, Size size, Type type, String name, String hearColor) {
        super(cost, size, type);
        this.name = name;
        this.hearColor = hearColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHearColor() {
        return hearColor;
    }

    public void setHearColor(String hearColor) {
        this.hearColor = hearColor;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(", Name - %s, Hear color - %s; \n", getName(), getHearColor());
    }
}
