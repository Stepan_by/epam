package by.epam.training.task1.room;

import by.epam.training.task1.collection.ToyList;
import by.epam.training.task1.toy.Toy;

import java.util.ArrayList;
import java.util.List;

public class GameRoom {

    private ToyList toys;

    public GameRoom() {
        toys = new ToyList();
    }

    public GameRoom(List<Toy> toys) {
        this.toys = new ToyList(toys);
    }

    public void addToy(Toy toy) {
        toys.add(toy);
    }

    public void addToys(ArrayList<Toy> toys) {
        this.toys.addAll(toys);
    }

    public ToyList getToys() {
        return toys.clone();
    }

    public int numberOfToys() {
        return toys.size();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        toys.stream().forEach(builder::append);
        return builder.toString();
    }
}
