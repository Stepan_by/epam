package by.epam.training.task1.factory;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.factory.exception.UnexpectedParameter;
import by.epam.training.task1.toy.Toy;
import by.epam.training.task1.toy.types.ball.LargeBall;
import by.epam.training.task1.toy.types.bricks.LargeBricks;
import by.epam.training.task1.toy.types.car.LargeBus;
import by.epam.training.task1.toy.types.doll.LargeKen;

public class LargeToyFactory {

    public static Toy createLargeToy(Toy.Type type) throws UnexpectedParameter, DataException {
        switch (type) {
            case CAR:
                return new LargeBus();
            case BRICKS:
                return new LargeBricks();
            case BALL:
                return new LargeBall();
            case DOLL:
                return new LargeKen();
            case ANY:
                return null;
            default:
                throw new UnexpectedParameter("Unknown type " + type.name());
        }
    }
}
