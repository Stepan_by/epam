package by.epam.training.task1.factory;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.factory.exception.UnexpectedParameter;
import by.epam.training.task1.toy.Toy;
import by.epam.training.task1.toy.types.ball.SmallBall;
import by.epam.training.task1.toy.types.bricks.SmallBricks;
import by.epam.training.task1.toy.types.car.SmallSportCar;
import by.epam.training.task1.toy.types.doll.SmallDoll;

public class SmallToyFactory {

    public static Toy createSmallToy(Toy.Type type) throws UnexpectedParameter, DataException {
        switch (type) {
            case CAR:
                return new SmallSportCar();
            case BRICKS:
                return new SmallBricks();
            case BALL:
                return new SmallBall();
            case DOLL:
                return new SmallDoll();
            case ANY:
                return null;
            default:
                throw new UnexpectedParameter("Unknown type " + type.name());
        }
    }
}
