package by.epam.training.task1.factory;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.factory.exception.UnexpectedParameter;
import by.epam.training.task1.toy.Toy;
import by.epam.training.task1.toy.types.ball.MediumBall;
import by.epam.training.task1.toy.types.bricks.MediumBricks;
import by.epam.training.task1.toy.types.car.SimpleMediumCar;
import by.epam.training.task1.toy.types.doll.MediumBarbie;

public class MediumToyFactory {

    public static Toy createMediumToy(Toy.Type type) throws UnexpectedParameter, DataException {
        switch (type) {
            case CAR:
                return new SimpleMediumCar();
            case BRICKS:
                return new MediumBricks();
            case BALL:
                return new MediumBall();
            case DOLL:
                return new MediumBarbie();
            case ANY:
                return null;
            default:
                throw new UnexpectedParameter("Unknown type " + type.name());
        }
    }
}
