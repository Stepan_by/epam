package by.epam.training.task1.factory.exception;

public class UnexpectedParameter extends Exception {
    public UnexpectedParameter() {
    }

    public UnexpectedParameter(String message) {
        super(message);
    }

    public UnexpectedParameter(String message, Throwable cause) {
        super(message, cause);
    }

    public UnexpectedParameter(Throwable cause) {
        super(cause);
    }

    public UnexpectedParameter(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
