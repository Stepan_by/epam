package by.epam.training.task1.factory;

import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.factory.exception.UnexpectedParameter;
import by.epam.training.task1.toy.Toy;

public class ToyFactory {
    
    public Toy createToy(Toy.Size size, Toy.Type type) throws UnexpectedParameter, DataException {
        switch (size) {
            case SMALL:
                return SmallToyFactory.createSmallToy(type);
            case MEDIUM:
                return MediumToyFactory.createMediumToy(type);
            case LARGE:
                return LargeToyFactory.createLargeToy(type);
            case ANY:
                return null;
            default:
                throw new UnexpectedParameter("Unknown size " + size.name());
        }
    }
}
