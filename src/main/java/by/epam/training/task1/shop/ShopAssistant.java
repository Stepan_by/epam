package by.epam.training.task1.shop;

import by.epam.training.task1.toy.Toy;

import java.util.Comparator;

public class ShopAssistant implements Comparator<Toy> {

    @Override
    public int compare(Toy firstToy, Toy secondToy) {
        if (firstToy.getCost() != secondToy.getCost()) {
            return firstToy.getCost() - secondToy.getCost();
        } else if (!secondToy.getType().equals(firstToy.getType())) {
            return secondToy.getType().ordinal() - firstToy.getType().ordinal();
        } else {
            return secondToy.getSize().ordinal() - firstToy.getSize().ordinal();
        }
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }
}