package by.epam.training.task1.shop;

import by.epam.training.task1.collection.ToyList;
import by.epam.training.task1.exception.DataException;
import by.epam.training.task1.factory.ToyFactory;
import by.epam.training.task1.factory.exception.UnexpectedParameter;
import by.epam.training.task1.toy.Toy;
import org.apache.log4j.Logger;

import java.util.Collections;

public class Shop {

    private static final Logger LOG = Logger.getLogger(Shop.class);
    private ToyList assortment;
    private ToyFactory factory;

    public Shop() throws UnexpectedParameter {
        assortment = new ToyList();
        factory = new ToyFactory();
        fillAssortment();
    }

    private void fillAssortment() throws UnexpectedParameter {
        try {
            for (Toy.Size size : Toy.Size.values()) {
                for (Toy.Type type : Toy.Type.values()) {
                    Toy toy = factory.createToy(size, type);
                    if (toy != null) {
                        assortment.add(toy);
                    }
                }
            }
        } catch (DataException exception) {
            throw new UnexpectedParameter(exception);
        }
        Collections.sort(assortment, new ShopAssistant());
    }

    public ToyList buyToys(int number, int money) {
        ToyList toys = new ToyList(assortment);
        int sum = toys.stream().mapToInt(Toy::getCost).sum();
        while ((sum > money || toys.size() > number) && toys.size() > 0) {
            sum -= toys.get(toys.size() -1).getCost();
            toys.remove(toys.size() - 1);
        }
        if (toys.size() == 0) {
            LOG.info("You are so poor that can't buy any toy");
        }
        return toys;
    }
}
