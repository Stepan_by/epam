package by.epam.training.task1.main;

import by.epam.training.task1.administration.RoomManager;
import by.epam.training.task1.exception.ConvertingException;
import by.epam.training.task1.exception.FileException;
import by.epam.training.task1.factory.exception.UnexpectedParameter;
import by.epam.training.task1.room.GameRoom;
import by.epam.training.task1.collection.ToyList;
import by.epam.training.task1.toy.Toy;
import by.epam.training.task1.worker.RoomWorker;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;

public class Main {

    static {
        new DOMConfigurator().doConfigure("config\\log4j.xml", LogManager.getLoggerRepository());
    }

    private static final Logger LOG = Logger.getLogger(Main.class);
    private static final String outputFile = "output.txt";
    private static final String inputFile = "input.txt";
    private static final String paramsFile = "params.txt";
    private static GameRoom room;

    public static void main(String[] args) {
        room = new GameRoom();
        try {
            fillRoomWithToys();
            sortToysBySize();
            filterToys();
        } catch (FileException | ConvertingException | UnexpectedParameter exception) {
            LOG.error(exception.getMessage());
        }
    }

    private static void fillRoomWithToys() throws UnexpectedParameter, ConvertingException, FileException {
        room.addToys(RoomManager.buyToys(inputFile));
        LOG.info("Room has been filled successfully with " + room.numberOfToys() + " toys");
        RoomManager.makeReport("Toys in room", room.toString(), outputFile, false);
    }

    private static void sortToysBySize() throws FileException {
        ToyList sortedToys = RoomWorker.sortToysBySize(room.getToys());
        LOG.info("ToyList sorted by size perfectly");
        RoomManager.makeReport("Sorted by size", sortedToys.toString(), outputFile, true);
    }

    private static void filterToys() throws FileException, ConvertingException {
        ArrayList<Toy> filteredToys = RoomWorker.findToysByParams(room.getToys(), RoomManager.takeSpecialToysParams(paramsFile));
        LOG.info("ToyList filtered by params from " + paramsFile + " file");
        RoomManager.makeReport("Founded by params in " + paramsFile, filteredToys.toString(), outputFile, true);
    }
}
