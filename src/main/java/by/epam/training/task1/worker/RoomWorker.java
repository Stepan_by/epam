package by.epam.training.task1.worker;

import by.epam.training.task1.exception.ConvertingException;
import by.epam.training.task1.exception.FileException;
import by.epam.training.task1.collection.ToyList;
import by.epam.training.task1.toy.Toy;

import java.util.List;
import java.util.StringTokenizer;

public class RoomWorker {

    public static ToyList sortToysBySize(List<Toy> toys) throws FileException {
        return toys.stream().
                sorted(new RoomComparator()).
                collect(ToyList::new, ToyList::add, ToyList::addAll);
    }

    public static ToyList findToysByParams(List<Toy> toys, String params) throws ConvertingException, FileException {
        StringTokenizer tokenizer = new StringTokenizer(params, ";");
        ToyParams toyParams = new ToyParams(tokenizer.nextToken(), tokenizer.nextToken(), tokenizer.nextToken());
        return toys.stream().
                filter(toy ->
                    (toy.getSize().equals(toyParams.getSize()) || toyParams.getSize() == Toy.Size.ANY) &&
                    (toy.getType().equals(toyParams.getType()) || toyParams.getType() == Toy.Type.ANY) &&
                    toy.getCost() >= toyParams.getMinCost() &&
                    toy.getCost() <= toyParams.getMaxCost()
                ).collect(ToyList::new, ToyList::add, ToyList::addAll);
    }

    private static class ToyParams {
        private int minCost;
        private int maxCost;
        private Toy.Size size;
        private Toy.Type type;

        public ToyParams(int minCost, int maxCost, Toy.Size size, Toy.Type type) {
            this.minCost = minCost;
            this.maxCost = maxCost;
            this.size = size;
            this.type = type;
        }

        public ToyParams(String cost, String size, String type) throws FileException, ConvertingException {
            costBorderInit(cost.substring(cost.indexOf(": ") + 2));
            setSize(size.substring(size.indexOf(": ") + 2));
            setType(type.substring(type.indexOf(": ") + 2));
        }

        public int getMinCost() {
            return minCost;
        }

        public void setMinCost(int minCost) throws FileException {
            if (minCost < 0) {
                throw new FileException("Min cost was typed incorrectly");
            }
            this.minCost = minCost;
        }

        public int getMaxCost() {
            return maxCost;
        }

        public void setMaxCost(int maxCost) throws FileException {
            if (maxCost < 0 || maxCost < minCost) {
                throw new FileException("Min cost was typed incorrectly");
            }
            this.maxCost = maxCost;
        }

        public Toy.Size getSize() {
            return size;
        }

        public void setSize(Toy.Size size) {
            this.size = size;
        }

        public void setSize(String size) throws FileException {
            try {
                setSize(size.equals("*") ? Toy.Size.ANY : Toy.Size.valueOf(size.toUpperCase()));
            } catch (IllegalArgumentException exception) {
                throw new FileException("In params size was typed incorrectly");
            }
        }

        public Toy.Type getType() {
            return type;
        }

        public void setType(Toy.Type type) {
            this.type = type;
        }

        public void setType(String type) throws FileException {
            try {
                setType(type.equals("*") ? Toy.Type.ANY: Toy.Type.valueOf(type.toUpperCase()));
            } catch (IllegalArgumentException exception) {
                throw new FileException("In params type was typed incorrectly");
            }
        }

        private void costBorderInit(String cost) throws ConvertingException, FileException {
            StringTokenizer tokenizer = new StringTokenizer(cost, "-");
            try {
                setMinCost(Integer.parseInt(tokenizer.nextToken()));
                setMaxCost(Integer.parseInt(tokenizer.nextToken()));
            } catch (NumberFormatException exception) {
                throw new ConvertingException("Cost in params.txt was typed incorrectly");
            }
        }
    }
}
