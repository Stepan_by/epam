package by.epam.training.task1.worker;

import by.epam.training.task1.toy.Toy;

import java.util.Comparator;

public class RoomComparator implements Comparator<Toy> {
    @Override
    public int compare(Toy firstToy, Toy secondToy) {
        if (!firstToy.getType().equals(secondToy.getType())) {
            return firstToy.getType().ordinal() - secondToy.getType().ordinal();
        } else {
            return firstToy.getSize().ordinal() - secondToy.getSize().ordinal();
        }
    }
}
