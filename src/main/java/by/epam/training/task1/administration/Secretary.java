package by.epam.training.task1.administration;

import by.epam.training.task1.exception.FileException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Secretary {

    public static String readFile(String path) throws FileException {
        try(Scanner scanner = new Scanner(new File(path))) {
            return scanner.nextLine();
        } catch (FileNotFoundException exception) {
            throw new FileException("File has not been found");
        }
    }

}
