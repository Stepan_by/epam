package by.epam.training.task1.administration;

import by.epam.training.task1.exception.ConvertingException;
import by.epam.training.task1.exception.FileException;
import by.epam.training.task1.factory.exception.UnexpectedParameter;
import by.epam.training.task1.shop.Shop;
import by.epam.training.task1.toy.Toy;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class RoomManager {

    public static ArrayList<Toy> buyToys(String path) throws FileException, ConvertingException, UnexpectedParameter {
        String params = Secretary.readFile(path);
        StringTokenizer tokenizer = new StringTokenizer(params, " ");
        try {
            int demandedNumberOfToys = Integer.parseInt(tokenizer.nextToken());
            int moneyForToys = Integer.parseInt(tokenizer.nextToken());
            return new Shop().buyToys(demandedNumberOfToys, moneyForToys);
        } catch (NumberFormatException exception) {
            throw new ConvertingException("Params in file is not correct");
        }
    }

    public static void makeReport(String theme, String report, String path, boolean isAddToFile) throws FileException {
        try(FileWriter writer = new FileWriter(path, isAddToFile)) {
            writer.write("\t" + theme.toUpperCase() + "\n");
            if (report.length() != 0) {
                writer.write(report + "\n\n");
            }
        } catch (FileNotFoundException exception) {
            throw new FileException("File has not been found");
        } catch (IOException exception) {
            throw new FileException("Unknown exception");
        }
    }

    public static String takeSpecialToysParams(String path) throws FileException {
        return Secretary.readFile(path);
    }
}
