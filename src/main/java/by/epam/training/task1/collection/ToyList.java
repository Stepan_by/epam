package by.epam.training.task1.collection;

import by.epam.training.task1.toy.Toy;

import java.util.ArrayList;
import java.util.Collection;

public class ToyList extends ArrayList<Toy> {

    public ToyList() {
    }

    public ToyList(int initialCapacity) {
        super(initialCapacity);
    }

    public ToyList(Collection<? extends Toy> c) {
        super(c);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        this.stream().forEach(builder::append);
        return builder.toString();
    }

    @Override
    public ToyList clone() {
        return (ToyList) super.clone();
    }
}
